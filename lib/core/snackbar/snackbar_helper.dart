import 'package:flutter/material.dart';

abstract class SnackbarHelper {
  static showMessage(BuildContext context, String message) {
    SnackBar snackBar = SnackBar(
        content: Text(
      message,
      textAlign: TextAlign.center,
    ));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
