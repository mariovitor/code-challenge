import 'package:code_challenge/layers/domain/usecases/calcular_area_minima_parede/calcular_area_minima_parede_usecase.dart';
import 'package:code_challenge/layers/domain/usecases/calcular_area_minima_parede/calcular_area_minima_parede_usecase_imp.dart';
import 'package:code_challenge/layers/domain/usecases/calcular_quantidade_de_tinta/calcular_quantidade_de_tinta_usecase.dart';
import 'package:code_challenge/layers/domain/usecases/calcular_quantidade_de_tinta/calcular_quantidade_de_tinta_usecase_imp.dart';
import 'package:code_challenge/layers/domain/usecases/escolher_latas_de_tinta/escolher_latas_de_inta_usecase_imp.dart';
import 'package:code_challenge/layers/domain/usecases/escolher_latas_de_tinta/escolher_latas_de_tinta_usecase.dart';
import 'package:code_challenge/layers/screen/controllers/parede_controller.dart';
import 'package:get_it/get_it.dart';

class Inject {
  static void init() {
    GetIt getIt = GetIt.I;

    //usecases
    getIt.registerLazySingleton<CalcularAreaMinimaParedeUsecase>(
        () => CalcularAreaMinimaParedeUsecaseImp());
    getIt.registerLazySingleton<CalcularQuantidadeDeTintaUsecase>(
        () => CalcularQuantidadeDeTintaUsecaseImp());
    getIt.registerLazySingleton<EscolherLatasDeTintaUsecase>(
        () => EscolherLatasDeTintaUsecaseImp());

    //Controllers

    getIt.registerFactory<ParedeController>(
        () => ParedeController(getIt(), getIt(), getIt()));
  }
}
