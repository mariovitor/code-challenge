import 'package:code_challenge/layers/domain/entities/parede_entity.dart';

abstract class CalcularQuantidadeDeTintaUsecase {
  double call(List<ParedeEntity> listaParedes);
}
