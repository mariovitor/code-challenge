import 'package:code_challenge/layers/domain/entities/parede_entity.dart';
import 'package:code_challenge/layers/domain/entities/tinta_entity.dart';

import 'calcular_quantidade_de_tinta_usecase.dart';

class CalcularQuantidadeDeTintaUsecaseImp
    implements CalcularQuantidadeDeTintaUsecase {
  @override
  double call(List<ParedeEntity> listaParedes) {
    double quantidadeTinta = 0;
    double area = 0;
    for (var parede in listaParedes) {
      area += parede.areaPintura;
    }
    quantidadeTinta =
        double.parse((area / TintaEntity.RENDIMENTO).toStringAsFixed(2));
    return quantidadeTinta;
  }
}
