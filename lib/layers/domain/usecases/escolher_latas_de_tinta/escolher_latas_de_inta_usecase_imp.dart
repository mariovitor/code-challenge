import '../../entities/tinta_entity.dart';
import 'escolher_latas_de_tinta_usecase.dart';

class EscolherLatasDeTintaUsecaseImp implements EscolherLatasDeTintaUsecase {
  @override
  Map<double, int> call(double litros) {
    Map<double, int> latasVsQuantidade = {};
    double litrosRestantes = litros;
    for (var element in TintaEntity.LITRAGENS) {
      if (litrosRestantes >= element) {
        int quantidadeLatas = (litrosRestantes / element).floor();
        litrosRestantes = litrosRestantes - (quantidadeLatas * element);
        latasVsQuantidade.addAll({element: quantidadeLatas});
      }
      //SE AINDA SOBROU ALGUMA LITRAGEM A SER PREENCHIDA ADICIONAR +1 LATA DE 0.5
      if (element == TintaEntity.LITRAGENS.last && litrosRestantes > 0) {
        if (latasVsQuantidade.containsKey(element)) {
          //Se já contem lata de 0.5 adicionar +1 Senão adicionar uma nova
          int latas = latasVsQuantidade[element]!;
          latasVsQuantidade[element] = latas + 1;
        } else {
          latasVsQuantidade.addAll({element: 1});
        }
        litrosRestantes =
            ((litrosRestantes - element) < 0) ? 0 : (litrosRestantes - element);
      }
    }
    return latasVsQuantidade;
  }
}
