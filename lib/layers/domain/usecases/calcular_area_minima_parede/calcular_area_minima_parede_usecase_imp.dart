import '../../entities/parede_entity.dart';
import 'calcular_area_minima_parede_usecase.dart';

class CalcularAreaMinimaParedeUsecaseImp
    implements CalcularAreaMinimaParedeUsecase {
  @override
  Map<String, dynamic> call(ParedeEntity parede) {
    double areaMinima = ParedeEntity.areaMinima;
    if (parede.areaPortasJanelas > 0) {
      areaMinima = (parede.areaPortasJanelas / 0.5);
    }
    bool temAreaMinima = (parede.areaBruta >= areaMinima) ? true : false;

    return {'areaMinima': areaMinima, 'temAreaMinima': temAreaMinima};
  }
}
