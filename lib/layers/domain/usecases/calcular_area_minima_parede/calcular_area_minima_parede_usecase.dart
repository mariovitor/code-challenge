import 'package:code_challenge/layers/domain/entities/parede_entity.dart';

abstract class CalcularAreaMinimaParedeUsecase {
  call(ParedeEntity parede);
}
