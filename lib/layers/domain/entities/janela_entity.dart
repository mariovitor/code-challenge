class JanelaEntity {
  static const double largura = 2;
  static const double altura = 1.2;
  static const double area = JanelaEntity.altura * JanelaEntity.largura;
}
