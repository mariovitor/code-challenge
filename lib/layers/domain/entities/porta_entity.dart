class PortaEntity {
  static const double altura = 1.9;
  static const double largura = 0.8;
  static const double area = PortaEntity.altura * PortaEntity.largura;
}
