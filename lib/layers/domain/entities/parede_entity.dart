import 'package:code_challenge/layers/domain/entities/janela_entity.dart';
import 'package:code_challenge/layers/domain/entities/porta_entity.dart';

class ParedeEntity {
  static const double areaMaxima = 15;
  static const double areaMinima = 1;
  static const alturaMinimaComPorta = PortaEntity.altura + 0.3;
  late double areaPintura;
  late double areaBruta;
  double altura;
  double largura;
  double areaPortasJanelas = 0;
  int janelas;
  int portas;
  bool temAlturaMinima = true;

  ParedeEntity({
    required this.altura,
    required this.largura,
    required this.janelas,
    required this.portas,
  }) {
    _calcularArea();
  }
  atualizar() {
    _calcularArea();
  }

  _calcularArea() {
    areaBruta = altura * largura;
    _calcularAreaPortasJanelas();
    areaPintura = areaBruta - areaPortasJanelas;
  }

  _calcularAreaPortasJanelas() {
    if (janelas > 0) {
      areaPortasJanelas = janelas * JanelaEntity.area;
    } else {
      areaPortasJanelas = 0;
    }
    if (portas > 0) {
      areaPortasJanelas = portas * PortaEntity.area;

      //Se houver porta, checar se parede tem altura minima
      if (altura < alturaMinimaComPorta) {
        temAlturaMinima = false;
      } else {
        temAlturaMinima = true;
      }
    } else {
      temAlturaMinima = true;
    }
  }
}
