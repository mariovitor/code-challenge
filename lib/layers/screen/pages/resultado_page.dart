import 'package:code_challenge/layers/screen/controllers/parede_controller.dart';
import 'package:flutter/material.dart';

class ResultadoPage extends StatelessWidget {
  const ResultadoPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ParedeController controller =
        ModalRoute.of(context)!.settings.arguments as ParedeController;

    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text('Quantidade de Tinta'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'Para pintar este cômodo você precisará de cerca de ${controller.quantidadeDeTinta}L de Tinta: ',
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline1,
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height / 5,
              ),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 22),
                child: Text(
                  'A melhor combinação de tintas é :',
                  style: TextStyle(fontSize: 18),
                ),
              ),
              Container(
                height: controller.latasEscolhidas.length * 35,
                decoration: BoxDecoration(
                    border: Border.all(),
                    borderRadius: BorderRadius.circular(26)),
                child: Padding(
                  padding: EdgeInsets.only(
                      top: controller.latasEscolhidas.length * 3),
                  child: ListView.builder(
                    itemCount: controller.latasEscolhidas.length,
                    itemBuilder: (context, index) {
                      return Text(
                        '${controller.quantidadeDasLatas[index].toString()} Latas de ${controller.latasEscolhidas[index].toString()}L',
                        textAlign: TextAlign.center,
                        style:
                            const TextStyle(fontSize: 20, letterSpacing: 1.2),
                      );
                    },
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Icon(Icons.arrow_back_rounded),
                      Text(
                        'Voltar',
                        style: TextStyle(fontSize: 18),
                      )
                    ],
                  )),
            ],
          ),
        ));
  }
}
