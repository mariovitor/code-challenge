// ignore_for_file: must_be_immutable

import 'package:code_challenge/layers/screen/controllers/parede_controller.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

import '../components/home_page/card_parede_component.dart';

class HomePage extends StatelessWidget {
  HomePage({Key? key}) : super(key: key);
  ParedeController paredeController = GetIt.I.get<ParedeController>();
  final GlobalKey<FormState> _formKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Calculadora de Tinta'),
      ),
      body: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'Preencha as informações sobre as paredes do cômodo a ser pintado:',
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline1,
              ),
              const SizedBox(
                height: 10,
              ),
              Expanded(
                child: Card(
                  child: Form(
                      key: _formKey,
                      child: ListView(
                        shrinkWrap: true,
                        children: [
                          cardParedeComponent(
                              1, paredeController.paredes[0], paredeController),
                          cardParedeComponent(
                              2, paredeController.paredes[1], paredeController),
                          cardParedeComponent(
                              3, paredeController.paredes[2], paredeController),
                          cardParedeComponent(
                              4, paredeController.paredes[3], paredeController),
                        ],
                      )),
                ),
              ),
              ElevatedButton(
                  onPressed: () {
                    _formKey.currentState!.validate();
                    if (paredeController.checkAreaMinima(context) &&
                        paredeController.checkAreaMaxima(context)) {
                      paredeController.calcularQuantidadeDeTinta();
                      Navigator.of(context)
                          .pushNamed('/resultado', arguments: paredeController);
                    }
                  },
                  child: const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text('CALCULAR'),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
