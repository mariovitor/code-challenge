// ignore_for_file: invalid_use_of_visible_for_testing_member, invalid_use_of_protected_member

import 'package:code_challenge/core/snackbar/snackbar_helper.dart';
import 'package:code_challenge/layers/domain/entities/parede_entity.dart';
import 'package:code_challenge/layers/domain/usecases/calcular_area_minima_parede/calcular_area_minima_parede_usecase.dart';
import 'package:code_challenge/layers/domain/usecases/calcular_quantidade_de_tinta/calcular_quantidade_de_tinta_usecase.dart';
import 'package:code_challenge/layers/domain/usecases/escolher_latas_de_tinta/escolher_latas_de_tinta_usecase.dart';
import 'package:flutter/cupertino.dart';

class ParedeController {
  List<ValueNotifier<ParedeEntity>> paredes = [
    ValueNotifier(ParedeEntity(altura: 0, largura: 0, janelas: 0, portas: 0)),
    ValueNotifier(ParedeEntity(altura: 0, largura: 0, janelas: 0, portas: 0)),
    ValueNotifier(ParedeEntity(altura: 0, largura: 0, janelas: 0, portas: 0)),
    ValueNotifier(ParedeEntity(altura: 0, largura: 0, janelas: 0, portas: 0))
  ];
  double quantidadeDeTinta = 0;
  List latasEscolhidas = [];
  List quantidadeDasLatas = [];
  final CalcularQuantidadeDeTintaUsecase _calcularQuantidadeDeTintaUsecase;
  final CalcularAreaMinimaParedeUsecase _calcularAreaMinimaParedeUsecase;
  final EscolherLatasDeTintaUsecase _escolherLatasDeTintaUsecase;
  ParedeController(this._calcularQuantidadeDeTintaUsecase,
      this._calcularAreaMinimaParedeUsecase, this._escolherLatasDeTintaUsecase);

  calcularQuantidadeDeTinta() {
    double quantidade = _calcularQuantidadeDeTintaUsecase([
      paredes[0].value,
      paredes[1].value,
      paredes[2].value,
      paredes[3].value,
    ]);
    quantidadeDeTinta = quantidade;
    _escolherLatas(quantidadeDeTinta);
  }

  _escolherLatas(double litros) {
    List _tempListEscolhidas = [];
    List _tempListQuantidade = [];
    Map latas = _escolherLatasDeTintaUsecase(litros);
    latas.forEach((key, value) {
      _tempListEscolhidas.add(key);
      _tempListQuantidade.add(value);
    });
    latasEscolhidas = _tempListEscolhidas;
    quantidadeDasLatas = _tempListQuantidade;
  }

  setAltura(String altura, ValueNotifier<ParedeEntity> paredeNotifier) {
    paredeNotifier.value.altura = double.parse(altura) / 100;
    paredeNotifier.value.atualizar();
    paredeNotifier.notifyListeners();
  }

  setLargura(String largura, ValueNotifier<ParedeEntity> paredeNotifier) {
    paredeNotifier.value.largura = double.parse(largura) / 100;
    paredeNotifier.value.atualizar();
    paredeNotifier.notifyListeners();
  }

  setJanelas(String janelas, ValueNotifier<ParedeEntity> paredeNotifier) {
    paredeNotifier.value.janelas = int.parse(janelas);
    paredeNotifier.value.atualizar();
    paredeNotifier.notifyListeners();
  }

  setPortas(String portas, ValueNotifier<ParedeEntity> paredeNotifier) {
    paredeNotifier.value.portas = int.parse(portas);
    paredeNotifier.value.atualizar();
    paredeNotifier.notifyListeners();
  }

  checkAreaMinima(BuildContext context) {
    int counter = 0;
    List<bool> paredesOk = [false, false, false, false];
    for (counter; counter < paredes.length; counter++) {
      paredes[counter].value.atualizar();
      Map check = _calcularAreaMinimaParedeUsecase(paredes[counter].value);
      //altura minima da parede
      if (paredes[counter].value.temAlturaMinima) {
        //String se houver janelas
        String parteFinal = paredes[counter].value.areaPortasJanelas > 0
            ? 'para ter essas portas e janelas'
            : '';

        if (check['temAreaMinima']) {
          paredesOk[counter] = true;
        } else {
          SnackbarHelper.showMessage(
              context,
              'A parede ${counter + 1} deve ter no minimo ${check['areaMinima']}m² ' +
                  parteFinal);
          break;
        }
      } else {
        SnackbarHelper.showMessage(context,
            'A Parede ${counter + 1} deve ter no minimo ${ParedeEntity.alturaMinimaComPorta.toStringAsFixed(2)} de altura para ter uma porta');
        break;
      }
    }
    return arrayChecker(paredesOk);
  }

  checkAreaMaxima(BuildContext context) {
    int counter = 0;
    List<bool> paredesOk = [false, false, false, false];

    for (counter; counter < paredes.length; counter++) {
      paredes[counter].value.atualizar();
      if (paredes[counter].value.areaBruta <= ParedeEntity.areaMaxima) {
        paredesOk[counter] = true;
      } else {
        SnackbarHelper.showMessage(context,
            'A area da Parede ${counter + 1} ultrapassou o limite de 15m²');
      }
    }
    return arrayChecker(paredesOk);
  }

  //metodo aux
  bool arrayChecker(List<bool> list) {
    if (list.contains(false)) {
      return false;
    } else {
      return true;
    }
  }
}
