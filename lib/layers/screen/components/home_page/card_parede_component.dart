import 'package:code_challenge/layers/domain/entities/parede_entity.dart';
import 'package:code_challenge/layers/screen/controllers/parede_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

cardParedeComponent(int a, ValueNotifier<ParedeEntity> paredeNotifier,
    ParedeController controller) {
  TextEditingController textEditingAltura = TextEditingController();
  TextEditingController textEditingLargura = TextEditingController();
  return Card(
    elevation: 7,
    child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: ValueListenableBuilder<ParedeEntity>(
          valueListenable: paredeNotifier,
          builder: (context, parede, widget) {
            return Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                RotatedBox(
                  quarterTurns: 135,
                  child: Text(
                    'Parede $a',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.grey.shade600,
                      fontWeight: FontWeight.w500,
                      letterSpacing: 1.1,
                      fontSize: 22,
                      decorationStyle: TextDecorationStyle.double,
                    ),
                  ),
                ),
                const SizedBox(
                  width: 20,
                ),
                Column(
                  children: [
                    SizedBox(
                      width: 80,
                      height: 40,
                      child: TextFormField(
                        autofocus: false,
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        validator: (value) {
                          if (value == null || value == '') {
                            return '';
                          } else if (int.parse(value) < 25) {
                            return '';
                          } else {
                            return null;
                          }
                        },
                        onChanged: (text) {
                          // ignore: unnecessary_null_comparison
                          if (text != '' && text != null) {
                            controller.setAltura(text, paredeNotifier);
                          }
                        },
                        controller: textEditingAltura,
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(4),
                          FilteringTextInputFormatter.deny(
                              RegExp(r'(\,)|(\ )|(\-)|(\.)'),
                              replacementString: ''),

                          // FilteringTextInputFormatter(RegExp(''),
                          //     allow: true)
                        ],
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            isDense: true,
                            errorStyle: const TextStyle(height: 0),
                            label: const Text('Altura (cm)'),
                            border: const OutlineInputBorder(),
                            filled: true,
                            fillColor: Colors.orange.shade100.withOpacity(0.1)),
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.all(4.0),
                      child: Text('X'),
                    ),
                    SizedBox(
                      width: 80,
                      height: 40,
                      child: TextFormField(
                        onChanged: (text) {
                          controller.setLargura(
                              (text != '' ? text : 1.toString()),
                              paredeNotifier);
                        },
                        controller: textEditingLargura,
                        validator: (value) {
                          if (value == null || value == '') {
                            return '';
                          } else if (int.parse(value) < 25) {
                            return '';
                          } else {
                            return null;
                          }
                        },
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(4),
                          FilteringTextInputFormatter.deny(
                              RegExp(r'(\,)|(\ )|(\-)|(\.)'),
                              replacementString: ''),

                          // FilteringTextInputFormatter(RegExp(''),
                          //     allow: true)
                        ],
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            isDense: true,
                            errorStyle: const TextStyle(height: 0),
                            label: const Text('Largura (cm)'),
                            border: const OutlineInputBorder(),
                            filled: true,
                            fillColor: Colors.orange.shade100.withOpacity(0.1)),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  width: 10,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Row(
                        children: [
                          const Text(
                            'Portas: ',
                            style: TextStyle(fontSize: 18),
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          DropdownButton<String>(
                              elevation: 2,
                              value: parede.portas.toString(),
                              items: <String>[
                                '0',
                                '1',
                                '2',
                                '3'
                              ].map<DropdownMenuItem<String>>((String valor) {
                                return DropdownMenuItem<String>(
                                  value: valor,
                                  child: Text(
                                    valor,
                                    style: const TextStyle(fontSize: 18),
                                  ),
                                );
                              }).toList(),
                              onChanged: (value) {
                                controller.setPortas(value!, paredeNotifier);
                              }),
                        ],
                      ),
                      Row(
                        children: [
                          const Text(
                            'Janelas: ',
                            style: TextStyle(fontSize: 18),
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          DropdownButton<String>(
                              elevation: 2,
                              value: parede.janelas.toString(),
                              items: <String>[
                                '0',
                                '1',
                                '2',
                                '3'
                              ].map<DropdownMenuItem<String>>((String valor) {
                                return DropdownMenuItem<String>(
                                  value: valor,
                                  child: Text(
                                    valor,
                                    style: const TextStyle(fontSize: 18),
                                  ),
                                );
                              }).toList(),
                              onChanged: (value) {
                                controller.setJanelas(value!, paredeNotifier);
                              }),
                        ],
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
              ],
            );
          }),
    ),
  );
}
