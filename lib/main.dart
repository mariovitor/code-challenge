import 'package:code_challenge/core/inject/inject.dart';
import 'package:code_challenge/layers/screen/pages/resultado_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'layers/screen/pages/homepage.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Inject.init();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Calculadora de Tinta',
      initialRoute: '/home',
      routes: {
        '/home': (context) => HomePage(),
        '/resultado': (context) => const ResultadoPage()
      },
      theme: ThemeData(
          primarySwatch: Colors.red,
          textTheme: const TextTheme(
              headline1: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.w300),
              subtitle1: TextStyle())),
    );
  }
}
