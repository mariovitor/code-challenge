import 'package:code_challenge/layers/domain/usecases/escolher_latas_de_tinta/escolher_latas_de_inta_usecase_imp.dart';
import 'package:code_challenge/layers/domain/usecases/escolher_latas_de_tinta/escolher_latas_de_tinta_usecase.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  EscolherLatasDeTintaUsecase escolherLatasDeTintaUsecaseImp =
      EscolherLatasDeTintaUsecaseImp();
  test("Deve fornecer 1 lata de 18L para os 18L necessários e nenhuma outra",
      () {
    double litros = 18;
    Map latas = escolherLatasDeTintaUsecaseImp(litros);
    expect(latas, {18: 1});
  });

  test(
      "Deve fornecer 1 lata de 18L e 1 de 0.5L para os 18.2L necessários e nenhuma outra",
      () {
    double litros = 18.2;
    Map latas = escolherLatasDeTintaUsecaseImp(litros);
    expect(latas, {18: 1, 0.5: 1});
  });

  test(
      "Deve fornecer 1 lata de 18L e 2 de 0.5L para os 18.6L necessários e nenhuma outra",
      () {
    double litros = 18.6;
    Map latas = escolherLatasDeTintaUsecaseImp(litros);
    expect(latas, {18: 1, 0.5: 2});
  });
}
