import 'package:code_challenge/layers/domain/entities/parede_entity.dart';
import 'package:code_challenge/layers/domain/usecases/calcular_area_minima_parede/calcular_area_minima_parede_usecase.dart';
import 'package:code_challenge/layers/domain/usecases/calcular_area_minima_parede/calcular_area_minima_parede_usecase_imp.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  CalcularAreaMinimaParedeUsecase calcularAreaMinimaParedeUsecase =
      CalcularAreaMinimaParedeUsecaseImp();
  test("A parede deve ter no mínimo 4.8m² para ter uma janela", () {
    ParedeEntity paredeEntity =
        ParedeEntity(altura: 1, largura: 1, janelas: 1, portas: 0);
    Map calculosArea = calcularAreaMinimaParedeUsecase(paredeEntity);
    expect(calculosArea['areaMinima'], 4.8);
  });
  test("Uma parede de 2x2 não tem a area minima pra uma janela", () {
    ParedeEntity paredeEntity =
        ParedeEntity(altura: 2, largura: 2, janelas: 1, portas: 0);
    Map calculosArea = calcularAreaMinimaParedeUsecase(paredeEntity);
    expect(calculosArea['temAreaMinima'], false);
  });
}
