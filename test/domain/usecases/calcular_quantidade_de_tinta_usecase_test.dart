import 'package:code_challenge/layers/domain/entities/parede_entity.dart';
import 'package:code_challenge/layers/domain/usecases/calcular_quantidade_de_tinta/calcular_quantidade_de_tinta_usecase.dart';
import 'package:code_challenge/layers/domain/usecases/calcular_quantidade_de_tinta/calcular_quantidade_de_tinta_usecase_imp.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  CalcularQuantidadeDeTintaUsecase calcularQuantidadeDeTintaUsecase =
      CalcularQuantidadeDeTintaUsecaseImp();
  test(
      "deve retornar 2litros de tinta para uma parede de 10m² sem janelas nem portas",
      () {
    ParedeEntity paredeEntity =
        ParedeEntity(altura: 2, largura: 5, janelas: 0, portas: 0);
    double litrosDeTinta = calcularQuantidadeDeTintaUsecase([paredeEntity]);
    expect(litrosDeTinta, 2);
  });

  test(
      "deve retornar 2litros de tinta para uma parede de 12.m² com 1 janela e 0 portas",
      () {
    ParedeEntity paredeEntity =
        ParedeEntity(altura: 2, largura: 6.2, janelas: 1, portas: 0);
    double litrosDeTinta = calcularQuantidadeDeTintaUsecase([paredeEntity]);
    expect(litrosDeTinta, 2);
  });

  test(
      "deve retornar 2.02litros de tinta para uma parede de 14m² com 1 janela e 1 portas",
      () {
    ParedeEntity paredeEntity =
        ParedeEntity(altura: 2, largura: 7, janelas: 1, portas: 1);
    double litrosDeTinta = calcularQuantidadeDeTintaUsecase([paredeEntity]);
    expect(litrosDeTinta, 2.02);
  });
}
