import 'package:code_challenge/layers/domain/entities/parede_entity.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  ParedeEntity paredeEntity =
      ParedeEntity(altura: 2, largura: 4, portas: 0, janelas: 0);

  test("deve retornar o m² da parede (8m²)", () {
    expect(paredeEntity.areaPintura, 8);
  });

  test("Deve retornar o m² da parede descontando uma janela (5.6m²)", () {
    ParedeEntity paredeEntityComJanela =
        ParedeEntity(altura: 2, largura: 4, janelas: 1, portas: 0);
    expect(paredeEntityComJanela.areaPintura, 5.6);
  });

  test("Deve retornar o m² da parede descontando uma porta (6.48m²)", () {
    ParedeEntity paredeEntityComPorta =
        ParedeEntity(altura: 2, largura: 4, janelas: 0, portas: 1);
    expect(paredeEntityComPorta.areaPintura, 6.48);
  });
}
