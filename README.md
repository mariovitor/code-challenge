# DIGITAL REPUBLIC CODE CHALLENGE
# CALCULADORA DE TINTA

## OBJETIVO
Calcular a quantidade de tinta necessária para pintar um cômodo com quatro paredes de medidas, quantidade de janelas e portas definidas pelo usuário via mobile.

##  REGRAS DE NEGÓCIO
1. Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 15 metros quadrados, mas podem possuir alturas e larguras diferentes
2. O total de área das portas e janelas deve ser no máximo 50% da área de parede
3. A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta
4. Cada janela possui as medidas: 2,00 x 1,20 mtos
5. Cada porta possui as medidas: 0,80 x 1,90
6. Cada litro de tinta é capaz de pintar 5 metros quadrados
7. Não considerar teto nem piso.
8. As variações de tamanho das latas de tinta são:
   - 0,5 L
   - 2,5 L
   - 3,6 L
   - 18 L

## PARA RODAR O PROJETO VOCÊ TEM DUAS OPÇÕES
### INSTALAR MOBILE VIA PLAYSTORE (ANDROID)

https://play.google.com/store/apps/details?id=com.mitrovapps.code_challenge


### RODAR VIA FLUTTER EM UM PC
```bash
- Você precisa ter instalado o Flutter, junto com suas dependências
- Você precisa ter instalado uma AVD ou emulador

1 - Faça o download da pasta
2 - Acesse a pasta via terminal e dentro dela rode "flutter run" sem aspas.
ou
2 - No terminal rode flutter run DIRETORIO/DA/PASTA
```

## USO DO APLICATIVO
Na pagina inicial se enconta 4 cards onde o usuário define os parâmetros de cada parede.
Ao clicar em calcular o aplicativo checa as validações
Se as validações estiverem certas o usuário é encaminhado para a próxima tela
Na próxima dela é exibida a quantidade aprox de tinta necessária e a melhor combinação de tamanhos de latas de tinta